const baseurl = 'https://shop.maohemao.com/mgr/'
let header = {
	'content-type': 'application/json',
	'openid': '',
	'sessionid': ''
}
function http(url,method,data){
	var value = wx.getStorageSync('sessionid');//同步获取本地储存
	if (value) {
		header.openid = value.openid
		header.sessionid = value.sessionid
  }
 
  // var timer=""
  return new Promise((resolve,reject)=>{
    wx.request({
      url: baseurl+url,
      method:method,
			data:{
				datas:data
      },
      success: (res => {
        wx.hideLoading();
        if(res.statusCode == 200){
          resolve(res.data)
        }else{
          reject(res)
        }
      }),
      fail: (res => {
        wx.hideLoading();
        wx.showToast({
          title: '网络出错',
          icon: 'none',
          duration: 1500
        })
      })
    })
  })
}
module.exports = {
	http
}