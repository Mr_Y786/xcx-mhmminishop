const ourl = 'https://shop.maohemao.com/mgr/'
let header = {
	'content-type': 'application/json',
	'openid': ''
}
// const header = 'application/x-www-form-urlencoded'//python后台
function login(){
	wx.login({
		success (even) {
			if (even.code) {
				wx.request({
					url: 'https://shop.maohemao.com/mgr/login/get_openid_key',
					method:'POST',
					data:{
						datas:even.code
					},
					header: {
						'content-type': 'application/json'
					},
					success: (res => {
						wx.hideLoading();
						wx.setStorageSync('sessionid', res.data.data);//同步储存
					}),
					fail: (res => {
						wx.hideLoading();
						wx.showToast({
							title: '网络出错',
							icon: 'none',
							duration: 1500
						})
					})
				})
			} else {
				console.log('登录失败！' + even.errMsg)
			}
		}
	})
}
// wx.getSetting({
// 	success (res){
// 		if (res.authSetting['scope.userInfo']) {
// 			wx.getUserInfo({
// 				success: function(res) {
// 					request('save_user_info','POST',res.userInfo).then(res => {
						
// 					})
// 				}
// 			})
// 		}
// 	}
// })
function request(url,method,data){
	var value = wx.getStorageSync('sessionid');//同步获取本地储存
	if (value) {
		header.openid = value.openid
		header.sessionid = value.sessionid
	}
	var timer=""
	let promise = new Promise((resolve,reject) => {
		wx.showLoading({
			title:'加载中'
		})
		wx.request({
			url: ourl + url,
			method:method,
			data:{
				datas:data
			},
			header: header,
			success: (res => {
				wx.hideLoading();
				if (res.statusCode == 200) {
					// if(res.data.code==3){
					// 	wx.showToast({
					// 		title: '请先登录',
					// 		icon:"none"
					// 	})
					// 	timer=setTimeout(() => {
					// 		wx.switchTab({
					// 			url: '/pages/my/my',
					// 		})
					// 	}, 1000);
					// }
					resolve(res);
				}else {
					reject(res.data);
				}
			}),
			fail: (res => {
				wx.hideLoading();
				wx.showToast({
					title: '网络出错',
					icon: 'none',
					duration: 1500
				})
				reject('网络出错');
			})
		})
	})
	return promise;
}

module.exports = {
	request: request,
	login:login
}

