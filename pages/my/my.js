// pages/my/my.js
const request = require("../../utils/request.js");
const app = getApp();
Page({

	/**
	* 页面的初始数据
	*/
	data: {
		code:false,
		canIUse: wx.canIUse('button.open-type.getUserInfo'),
		data:{
			name:'',
			url:'../image/toux.png'
		},
		state:[
			{
				name:'待付款',
				src:'01.png'
			},
			{
				name:'待发货',
				src:'02.png'
			},
			{
				name:'待收货',
				src:'03.png'
			},
			{
				name:'待评价',
				src:'04.png'
			},
			{
				name:'售后服务',
				src:'05.png'
			}
		],
		pages:["../address/address","../member/member","../coupon/coupon"]
	},

	/**
	* 生命周期函数--监听页面加载
	*/
	onLoad: function () {
		const session=wx.getStorageSync('sessionid')
		if(session.status==1){  //判断是否登录
			//已登陆显示
			request.request('user/get_user_info','POST').then(res => {
				this.data.data.name = res.data.data.nickname
				this.data.data.url = res.data.data.avatarurl
						this.setData({
							data:this.data.data,
							code:true
						})
			})
		}
		// else{
			//未登录
		// let that = this
		// request.login()
		// wx.getSetting({
		//   success (res){
		// 	if (!res.authSetting['scope.userInfo']) {
		// 		that.data.code = false
		// 	}else{
		// 		wx.getUserInfo({  //获取用户信息
		// 			success: function(res) {
		// 				that.data.data.name = res.userInfo.nickname
		// 				that.data.data.url = res.userInfo.avatarUrl
		// 				that.setData({
		// 					data:that.data.data,
		// 					code:true
		// 				})
		// 				//向后台发送用户信息  返回状态码  假如本地存储
		// 				request.request('user/save_user_info','POST',res.userInfo).then(res => {
		// 					wx.setStorageSync('sessionid', res.data.data)
		// 				})
		// 			}
		// 		})
		// 	}
		//   }
		// })
		// }
		
	},
	state:function(e){
		let id = e.currentTarget.dataset.id
		if(id == undefined){
			id = -1
		}
		let sessionid=wx.getStorageSync("sessionid")
		if(sessionid.status==1){
			wx.navigateTo({
				url: '../state/state?id=' + (id + 1)
			})
		}else{
			wx.showToast({
				title: '请登录',
				icon:"none"
			})
		}
		
	},
	xiugai:function(){
		wx.getSetting({
		  success (res){
			if (res.authSetting['scope.userInfo']) {
				wx.navigateTo({
				  url: '../modify/modify'
				})
			}else{
				wx.showToast({
					title: '暂未登录，无法操作',
					icon: 'none',
					duration: 1000
				})
			}
		  }
		})
	},
	//授权
	// openSetting:function(){
	// 	let that = this
	// 	wx.openSetting({
	// 	  success (res) {
	// 			console.log(res,"succ")
	// 		if(!(res.authSetting['scope.userInfo'])){
	// 			let data = {
	// 				name:'',
	// 				url:'../image/toux.png'
	// 			}
	// 			that.setData({
	// 				data:data,
	// 				code:false
	// 			})
	// 		}else{
	// 			console.log(111)
	// 			wx.getUserInfo({
	// 				success: function(res) {
	// 					that.data.data.name = res.userInfo.nickName
	// 					that.data.data.url = res.userInfo.avatarUrl
	// 					that.setData({
	// 						data:that.data.data,
	// 						code:true
	// 					})
	// 					request.request('user/save_user_info','POST',res.userInfo).then(res => {
							
	// 					})
	// 				}
	// 			})
	// 		}
	// 		},
	// 		fail(err){
	// 			console.log(err,"err")
	// 		}
	// 	})
	// },


	// 点击登录
	bindGetUserInfo (e) {
		let that = this
		if(e.detail.userInfo){
			that.data.data.name = e.detail.userInfo.nickName
			that.data.data.url = e.detail.userInfo.avatarUrl
			that.setData({
				data:that.data.data,
				code:true
			})
			request.request('user/save_user_info','POST',e.detail.userInfo).then(res => {
					wx.showToast({
						title: '登录成功',
						icon: 'none',
						duration: 1000
					})
					let sessionid=wx.getStorageSync("sessionid")
					sessionid.status=1;
					wx.setStorageSync('sessionid', sessionid)
					// setTimeout(()=>{
					// 	wx.navigateBack()
					// },1000)
				
			}).catch(err=>{
				wx.showToast({
					title: '登录失败',
					icon: 'none',
					duration: 1000
				})
			})
			
		}else{
		}
	},
	//点击跳转
	next(e){
		let sessionid=wx.getStorageSync("sessionid")
				if(sessionid.status==1){
					if(e.currentTarget.dataset.index==0){
						wx.navigateTo({
							url: '../address/address',
						})
					}else if(e.currentTarget.dataset.index==1){
						wx.navigateTo({
							url: '../member/member',
						})
					}else if(e.currentTarget.dataset.index==2){
						wx.navigateTo({
							url: '../coupon/coupon',
						})
					}
				}else{
					wx.showToast({
						title: '请登录',
						icon:"none"
					})
				}
	},

	/**
	* 生命周期函数--监听页面初次渲染完成
	*/
	onReady: function () {
		
	},

	/**
	* 生命周期函数--监听页面显示
	*/
	onShow: function () {
		
	},

	/**
	* 生命周期函数--监听页面隐藏
	*/
	onHide: function () {

	},

	/**
	* 生命周期函数--监听页面卸载
	*/
	onUnload: function () {

	},

	/**
	* 页面相关事件处理函数--监听用户下拉动作
	*/
	onPullDownRefresh: function () {

	},

	/**
	* 页面上拉触底事件的处理函数
	*/
	onReachBottom: function () {

	},

	/**
	* 用户点击右上角分享
	*/
	onShareAppMessage: function () {

	}
})